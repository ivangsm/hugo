---
title: "About Me"
date: 2019-10-29T13:49:23+06:00
draft: false

# image
image: "images/author.jpg"

# meta description
description: "Tech blog"

# type
type : "about"
---

# I spend my time learning and writing code to make great things with ☕ and a lot of ❤️