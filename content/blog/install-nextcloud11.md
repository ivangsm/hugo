---
title: "Instalar Nexcloud 11 usando Caddy en Ubuntu"
date: 2017-04-07T15:20:11-05:00
draft: false

# post thumb
image: "images/featured-post/nextcloud.png"

# meta description
description: "this is meta description"

# taxonomies
categories: 
  - "Web Server"
tags:
  - "Ubuntu"
  - "Linux"
  - "Nexcloud"

# post type
type: "post"
---

Nextcloud es un fork de ownCloud se utiliza para montar un sistema de almacenamiento de datos parecido a Dropbox, pero en tu propio servidor.

La base de datos que usaremos será MariaDB derivada de MySQL por lo que tiene una alta compatibilidad y varias ventajas.

Primero que nada, tendremos que instalar Caddy Server, si no saben cómo hacerlo vayan a este post [Instalar Caddy Server en Ubuntu](https://blog.ivansalazar.org/post/caddyphplinux/).

Instalaremos MariaDB

```
sudo apt update && sudo apt install mariadb-server
```

Una vez instalado crearemos una base de datos para Nextcloud

Nos logueamos como root

```
sudo su
```

después ingresaremos a la consola de MariaDB

```
mysql
```

Para crear la base de datos introduciremos estos comandos

```sql
CREATE DATABASE nextcloud;
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'localhost' IDENTIFIED BY 'ContraseñaNextcloud';
FLUSH PRIVILEGES;
\q
```

Terminámos la sesión root

``` 
exit 
```

Instalaremos los módulos PHP necesarios

```sh
sudo apt-get -y install php-fpm php-cli php-json php-curl php-imap php-gd php-mysql php-xml php-zip php-intl php-mcrypt php-imagick php-mbstring
```

Descargaremos Nextcloud, lo extraeremos lo moveremos a /www

```sh
cd /tmp
wget https://download.nextcloud.com/server/releases/latest-11.tar.bz2
tar -vxjf latest-11.tar.bz2
sudo mv nextcloud /var/www/nextcloud
```

Pondremos nuestros datos en un sitio diferente

```sh
sudo mkdir /mnt/nextcloud /mnt/nextcloud/data
sudo chown -R www-data: /mnt/nextcloud
```

Añadiremos esto a nuestro Caddyfile

```sh
nuestrodominio.com {

	root   /var/www/nextcloud
	log    /var/log/nextcloud_access.log
	errors /var/log/nextcloud_errors.log

	fastcgi / 127.0.0.1:9000 php {
		env PATH /bin
	}

	rewrite {
		r ^/index.php/.*$
		to /index.php?{query}
	}

	# client support (e.g. os x calendar / contacts)
	redir /.well-known/carddav /remote.php/carddav 301
	redir /.well-known/caldav /remote.php/caldav 301

	# remove trailing / as it causes errors with php-fpm
	rewrite {
		r ^/remote.php/(webdav|caldav|carddav|dav)(\/?)$
		to /remote.php/{1}
	}

	rewrite {
		r ^/remote.php/(webdav|caldav|carddav|dav)/(.+?)(\/?)$
		to /remote.php/{1}/{2}
	}

	# .htaccess / data / config / ... shouldn't be accessible from outside
	status 403 {
		/.htacces
		/data
		/config
		/db_structure
		/.xml
		/README
	}

	header / Strict-Transport-Security "max-age=31536000;"

}
```

Reemplazamos *nuestrodominio.com* de acuerdo al dominio o subdominio que utilicemos.

Y ya podremos usar Nextcloud, en caso de que al iniciarlo te dé un error de permisos tendremos que generar y ejecutar el siguiente script.

```
cd
nano nextcloud.sh
```

Y escribimos dentro

```sh
#!/bin/bash
ocpath='/var/www/nextcloud'
htuser='www-data'
htgroup='www-data'
rootuser='root'

printf "Creating possible missing Directories\n"
mkdir -p $ocpath/data
mkdir -p $ocpath/updater

printf "chmod Files and Directories\n"
find ${ocpath}/ -type f -print0 | xargs -0 chmod 0640
find ${ocpath}/ -type d -print0 | xargs -0 chmod 0750

printf "chown Directories\n"
chown -R ${rootuser}:${htgroup} ${ocpath}/
chown -R ${htuser}:${htgroup} ${ocpath}/apps/
chown -R ${htuser}:${htgroup} ${ocpath}/config/
chown -R ${htuser}:${htgroup} ${ocpath}/data/
chown -R ${htuser}:${htgroup} ${ocpath}/themes/
chown -R ${htuser}:${htgroup} ${ocpath}/updater/

chmod +x ${ocpath}/occ

printf "chmod/chown .htaccess\n"
if [ -f ${ocpath}/.htaccess ]
 then
  chmod 0644 ${ocpath}/.htaccess
  chown ${rootuser}:${htgroup} ${ocpath}/.htaccess
fi
if [ -f ${ocpath}/data/.htaccess ]
 then
  chmod 0644 ${ocpath}/data/.htaccess
  chown ${rootuser}:${htgroup} ${ocpath}/data/.htaccess
fi
```

Presionamos **CTRL+O** para guardar **ENTER** para aceptar y **CTRL+X** para salir.

Una vez hecho esto daremos permisos de ejecución.

```sh
chmod +x nextcloud.sh
```

Y ejecutaremos con

```sh
sudo ./nextcloud.sh
```

Y listo, deberiamos de tener Nexcloud corriendo en nuestro servidor Caddy.

Si tienes alguna duda, comentario o sugerencia, envia un mail a [admin@ivansalazar.org](mailto:admin@ivansalazar.org)