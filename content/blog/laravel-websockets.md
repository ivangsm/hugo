---
title: "Laravel-Websockets y Vue"
date: 2020-04-20T00:15:50-05:00
draft: false

# post thumb
image: "images/featured-post/laravel.jpg"

# meta description
description: "this is meta description"

# taxonomies
categories:
  - "Programming"
tags:
  - "Vue"
  - "Laravel"
  - "Websockets"
  - "Programming"

# post type
type: "post"
---

Los websockets son una parte fundamental de la web moderna, sobre todo si estás programando una SPA (Single Page Application).

Si tienes un proyecto o una API con Laravel y quieres agregar esta funcionalidad aquí te explicaré como configurar correctamente esta librería en tu proyecto y consumirla en tu proyecto.

Antes que nada, es necesario tener instalados los siguientes requerimientos:

- PHP 7.*
- composer
- Node
- Yarn/NPM

Crearemos la carpeta que contendrá nuestro proyecto tanto el frontend como en backend en este caso.

```bash
mkdir websockets-project && cd websockets-project
```

Una vez dentro del directorio crearemos nuestra aplicación de Laravel, yo utilizaré la version 6 ya que es la versión LTS más reciente.

```bash
composer create-project laravel/laravel="6.*" backend
```

E instalaremos la librerias necesarias.

```bash
composer require beyondcode/laravel-websockets
composer require pusher/pusher-php-server "~3.0"
```

Despues necesitaremos publicar el archivo de la migración necesario para laravel-websockets

```bash
php artisan vendor:publish --provider="BeyondCode\LaravelWebSockets\WebSocketsServiceProvider" --tag="migrations"
```

Despues de esto crearemos la base de datos que utilizaremos si es que no lo hemos hecho y configuraremos nuestro archivo **.env** para poder ejecutar correctamente las migraciones sobre esa base de datos.

 ```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=websockets
DB_USERNAME=root
DB_PASSWORD=
 ```
 
 Y simplemente ejecutamos la migración
 
 `php artisan migrate`
 
 Acto seguido publicaremos el archivo de configuración de websockets
 
 ```bash
 php artisan vendor:publish --provider="BeyondCode\LaravelWebSockets\WebSocketsServiceProvider" --tag="config"
 ```
 
 Ahora en nuestro archivo **.env** nuevamente necesitaremos agregar las configuraciones de drivers para el broadcast, le haremos creer que usará un servidor de Pusher aunque no sea así:
 
```bash
...
BROADCAST_DRIVER=pusher
...
PUSHER_APP_ID=12345
PUSHER_APP_KEY=ASDASD2121
PUSHER_APP_SECRET=ASDASD123123
PUSHER_APP_CLUSTER=mt1
```

Despues en el archivo **/config/app.php** descomentaremos la linea *App\Providers\BroadcastServiceProvider::class,*

```php
...
/*
 * Application Service Providers...
 */
App\Providers\AppServiceProvider::class,
App\Providers\AuthServiceProvider::class,
App\Providers\BroadcastServiceProvider::class, // <- Esta
App\Providers\EventServiceProvider::class,
App\Providers\RouteServiceProvider::class,
...
```

Ahora en el archivo **/config/broadcasting.php** agregaremos unas lineas para que la configuración de pusher nos quede justo así:

```php
...
'pusher' => [
            'driver' => 'pusher',
            'key' => env('PUSHER_APP_KEY'),
            'secret' => env('PUSHER_APP_SECRET'),
            'app_id' => env('PUSHER_APP_ID'),
            'options' => [
                'cluster' => env('PUSHER_APP_CLUSTER'),
                'useTLS' => true,
                'host' => '127.0.0.1', // <- Se agrega
                'port' => 6001, // <- Se agrega
                'scheme' => 'http' // <- Se agrega
            ],
        ],
...
```

Aquí ya tendremos el servidor listo y configurado para correr y crear eventos, podemos verificar que se ejecute correctamente con:

```bash
php artisan serve
php artisan websockets:serve
```

Al ingresar a **localhost:8000** veremos nuestra pagina del proyecto de Laravel y en **localhost:8000/laravel-websockets** veremos un dashboard donde podremos ver estadísticas de eventos y crear y ejecutar algunos manualmente.

Ahora crearemos un evento
 
 ```bash
 php artisan make:event NewMessage
 ```
 
 Esto creará el archivo **/App/Events/NewMessage.php** en el modificaremos algunas cosas:
 
 ```php
 <?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewMessage implements ShouldBroadcast // <- Esta linea debe quedar igual
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $message; // <- Creamos una variable publica

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message) // <- Agregamos un parametro
    {
        $this->message = $message; // <- Igualamos la variable publica al valor del parametro
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        //return new PrivateChannel('channel-name');
        return new Channel('home'); // <- Crea un canal público con nombre home
    }
}
 ```
 
 Después de esto corremos nuestros dos servidores el de la app y de websockets y abrimos tinker para mandar llamar nuestro evento.
 
 ```bash
 php artisan tinker
 ```
 
 Esto abrira una consola interactiva en la cual ejecutaremos el siguiente comando para verificar que el evento se ejecute:
 
 ```php
 event(new App\Events\NewMessage("Hola Mundo"))
 ```
 
 Esto debería mostrar un evento al inicio del listado de eventos en el dashboard de laravel-websockets, para verificarlo desde el mismo dashboard, en el formulario que aparece lo siguiente:
 
 ```bash
 Chanel=home
 Event=App\Events\NewMessage
 Data={"message": "Hola Mundo"}
 ```
 
 Y una ves ejecutado debería aparecer el evento.
 
 Hasta aquí tenemos configurado nuestro backend que podremos extender mediante funciones, eventos etc.
 
 Para verificar el funcionamiento en nuestro frontend crearemos una app en **Vue** en la raíz de la carpeta que contiene el proyecto es decir en **websockets-project** no en backend.
 
 ```bash
 vue create frontend
 ```
 
 Dejamos los valores por defecto y una ves instalado entramos a la carpeta frontend para agregar algunas librerías necesarias:
 
 ```bash
 cd frontend
 yarn add laravel-echo pusher-js
 ```
 
 Dentro de frontend en el archivo **App.vue** modificaremos unas cosas para verificar que todo esté funcionando correctamente.
 
 ```javascript
...
 <script>
import HelloWorld from './components/HelloWorld.vue'
import Echo from 'laravel-echo' // <- Importamos la libreria
window.Pusher = require('pusher-js') // <- Importamos la libreria

export default {
  name: 'App',
  components: {
    HelloWorld
  },
  mounted () { // <- Agregar la función mounted() con su lógica
    window.Echo = new Echo({
      broadcaster: 'pusher',
      key: 'ASDASD2121',
      wsHost: window.location.hostname,
      wsPort: 6001,
      disableStats: true
    })
    window.Echo.channel('home').listen('NewMessage', (e) => {
      console.log(e)
    })
  }
}
</script>
...
```

Una vez hecho esto y con los proyectos de Laravel ejecutandose, iniciaremos el proyecto de Vue y entraremos en nuestro navegador a la url **localhost:8081** o la URL que te entrega por defecto.
 
 Abrimos la consola del navegador con F12 y en apartado de console podremos ver el mensaje que ejecutemos mediante tinker o mediante el dashboard de Laravel-Websockets.
