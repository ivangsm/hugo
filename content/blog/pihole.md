---
title: "Bloquear Publicidad con Pi Hole"
date: 2019-09-02T00:21:54-05:00
draft: false

# post thumb
image: "images/featured-post/pihole.jpg"

# meta description
description: "this is meta description"

# taxonomies
categories: 
  - "Web Server"
tags:
  - "PiHole"
  - "Ads"

# post type
type: "post"
---

Pi-Hole es un bloqueador de anuncios mediante una instalación sobre hardware, funciona como un servidor DNS tomando las peticiones que haces desde tu navegador, pasa el contenido por filtros bloqueadores de publicidad y te entrega en contenido solicitado libre de anuncios publicitarios sin necesidad de tener software adicional instalado en tu pc.  

Los requerimientos son los siguientes:  

*   Una de las siguientes distribuciones Linux:
    *   Ubuntu
    *   Debian
    *   Fedora
    *   CentOS
*   Una pc para instalar dicho sistema

Personalmente y lo que recomiendo es utilizar una placa de desarrollo dado su bajo coste y consumo de energía ya sea una **Raspberry Pi, Orange Pi o similares**. Yo utilize una **Orange Pi PC** y **Armbian** como sistema operativo que está basado en debian.  

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525670959/Orange_Pi_Pc-800x800.jpg) Una vez que tenemos nuestro equipo listo procederemos a instalar Pi-Hole, su instalación es muy simple ya que bastará con pegar un comando en la terminal y seguir las instrucciones.  

Nos logueamos como root:  

```sh
sudo su
```   

Después empezaremos la instalación con:  

```sh
curl -sSL https://install.pi-hole.net | bash
```

Al iniciar la instalación nos aparecerá un anuncio adelantandonos lo que hará daremos enter  

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525666771/Pi-Hole/2.png) 

En la siguiente pantalla nos dirá que necesita tener configurada una IP estática para funcionar correctamente que podremos configurar más adelante en caso de tener configurado DHCP y no una IP estática.  

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525666771/Pi-Hole/3.png) 

Después deberemos configurar los servidores DNS te da varias opciones sin embargo yo utilizo y recomiendo usar los nuevos servidores DNS de Cloudflare que son rápidos y anónimos, para configurarlos vamos hasta la ultima opcion que dice “custom” y damos enter  

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525666771/Pi-Hole/4.png) 

Los servidores DNS de Cloudflare son **1.1.1.1 y 1.0.0.1** los deberemos escribir separados por comas con en la siguiente imagen  

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525666771/Pi-Hole/5.png) 

Seguido de esto confirmamos pulsando la tecla enter  

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525666771/Pi-Hole/6.png) 

Después nos pide seleccionar sobre qué protocolos bloquear los anuncios si no sabes lo que es IPv6 o no haces uso de él lo deseleccionamos poniéndonos sobre él y presionando la tecla espacio después aceptamos pulsando enter  

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525666771/Pi-Hole/7.png) 

En el siguiente paso nos dirá si queremos usar las IP actuales poniéndolas como estáticas, pulsamos enter de ser así, en caso contrario seleccionamos no y configuramos manualmente la IP estatica que querramos usar, **NO** debe estar en uso en nuestro router o podría generar problemas.

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525666771/Pi-Hole/8.png) 

Aquí nos mostrará una advertencia sobre lo antes dicho, si pones una IP en uso podría causar un conflicto si tu router no evita colisiones IP  

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525666771/Pi-Hole/9.png) 

Después nos dice si queremos instalar una interfaz de administración WEB, esto es útil para saber las estadísticas y controlar algunos aspectos mediante una interfaz gráfica WEB  

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525666771/Pi-Hole/10.png) 

Seguidamente nos preguntará si queremos guardar logs de las consultas si no instalaste la interfaz web y/o no te interesa saber los datos de bloqueos entre otras cosas puedes desactivarlo.  

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525666771/Pi-Hole/11.png) 

Comenzará a descargar e instalar los programas necesarios esperamos a que finalize no debe tardar más de 3 minutos dependiendo de la velocidad de tu internet  

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525666771/Pi-Hole/12.png) 

Si hay un firewall activado nos pedirá instalar reglas para IPTables para que Pi-Hole pueda funcionar, ya que hace uso de puertos HTTP y DNS  

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525666771/Pi-Hole/13.png) 

Después de eso habremos finalizado, nos mostrará una página con la dirección para acceder a nuestra web de administración así como una contraseña para poder acceder a la administración sin ella solo veremos datos básicos  

![](https://res.cloudinary.com/ivansalazar/image/upload/v1525666771/Pi-Hole/14.png) 

Nuestra interfaz de administración web se vera asi  

![](https://camo.githubusercontent.com/a8c384533f39f6eb47085bdc26b62464fe7da578/68747470733a2f2f70692d686f6c652e6769746875622e696f2f67726170686963732f53637265656e73686f74732f64617368626f6172642e706e67) 

Una vez hecho esto solo queda configurar nuestros dispositivos para que usen nuestro Pi-Hole como servidor DNS, esto de puede hacer de varias formas una es individualmente en cada dispositivo sin embargo lo más recomendable es hacer un uso global configurando nuestro Pi-Hole desde el router sin embargo no todos tienen esta opción.  

Para configurarlo en tu router bastará con cambiar los servidores DNS en tu interfaz LAN o en el apartado DHCP  

Para configurarlo en tu dispositivo individual bastará con buscar como configurar DNS en tu sistema operativo (Si configuraste tu router no es necesario hacer esto)  

Si tienes dudas acerca de este y cualquier otro tutorial en esta web no dude en enviar un email a [admin@ivansalazar.org](mailto:admin@ivansalazar.org) y te respondere en cuanto pueda hacerlo.
