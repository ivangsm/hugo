---
title: "Caddy2"
date: 2020-11-23T05:25:18-06:00
draft: true

# post thumb
image: "images/post/caddy.png"

# meta description
description: "this is meta description"

# taxonomies
categories:
  - "Web Server"
tags:
  - "Caddy"
  - "Linux"
  - "Ubuntu"

# post type
type: "featured"
---

Hace tiempo realizé un post explicando como instalar la versión 1 de Caddy un servidor web en ese entonces nuevo escrito en Go con una increible facilidad para configuirar, se ha publicado hace un tiempo la versión 2 de este servidor y me gustaria comentar sus novedades y como en el antiguo post las instrucciones para su instalación y manejo

Para realizar la instalación se agregaron repositorios para las distribuciones más populares de GNU/Linux

Así para su instalación en Ubuntu necesitaremos ejecutar los siguientes comandos para tenerlo casi completamente listo y configurado

```sh
echo "deb [trusted=yes] https://apt.fury.io/caddy/ /" \
    | sudo tee -a /etc/apt/sources.list.d/caddy-fury.list
sudo apt update
sudo apt install caddy
```